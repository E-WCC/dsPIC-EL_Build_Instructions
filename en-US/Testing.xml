<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % BOOK_ENTITIES SYSTEM "Construction_Instructions.ent">
%BOOK_ENTITIES;
]>
<section id="sect-test">
  <title>Testing the Board</title>
  <para>
    The supplied dsPIC33EV32GM002 comes preprogrammed with a test
    which exercises many of the features of the board.  Fully
    executing the test requires user action.
  </para>
  <formalpara>
    <title>Initial Startup</title>
    <para>
      The test begins with a two-second delay, during which time white
      blocks may appear on the LCD.  Following this delay, a start up
      banner is displayed for two seconds.
      <figure float="0" id="fig-startup">
	<title>Startup banner</title>
	<mediaobject>
          <imageobject><imagedata scale="35" scalefit="1" 
          fileref="images/OpeningDisplay.jpg" format="JPG"/>
          </imageobject>
          <textobject>
            <para>
              dsPIC-EL-GM Test
            </para>
          </textobject>
	</mediaobject>
      </figure>
    </para>
  </formalpara>

  <formalpara>
    <title>Initial Button Test</title>
    <para>
      A test is then made for the un-pressed buttons.  Do not press
      buttons during this brief test.  If a button fails, the number
      of the failed button is displayed.  This test simply checks that
      none of the buttons are shorted to ground.
      <figure float="0" id="fig-buttonfail">
	<title>Button failed</title>
	<mediaobject>
          <imageobject><imagedata scale="30" scalefit="1" 
          fileref="images/ButtonsFailed.jpg" format="JPG"/>
          </imageobject>
          <textobject>
            <para>
              Buttons Failed SW2
            </para>
          </textobject>
	</mediaobject>
      </figure>
    </para>
  </formalpara>
  <para>
    If the button test was successful, a message is displayed to
    that effect.
    <figure float="0" id="fig-buttonsok">
      <title>Initial button test passed</title>
      <mediaobject>
        <imageobject><imagedata scale="35" scalefit="1" 
        fileref="images/ButtonsGood.jpg" format="JPG"/>
        </imageobject>
        <textobject>
          <para>
            Initial check buttons good
          </para>
        </textobject>
      </mediaobject>
    </figure>
  </para>

  <formalpara>
    <title>LED Test</title>
    <para>
      <figure float="0" id="fig-led-test-banner">
	<title>LED test banner</title>
	<mediaobject>
          <imageobject><imagedata scale="35" scalefit="1" 
          fileref="images/CycleLEDs.jpg" format="JPG"/>
          </imageobject>
          <textobject>
            <para>
              Cycle LEDs
            </para>
          </textobject>
	</mediaobject>
      </figure>
      The LEDs are then tested.  The LCD display will match those LEDs
      which should be illuminated.  The test cycles through the
      following:
      <itemizedlist spacing="compact">
	<listitem>
	  <para>
	    LED1 is illuminated
	  </para>
	</listitem>
	<listitem>
	  <para>
	    LED2 is illuminated
	  </para>
	</listitem>
	<listitem>
	  <para>
	    LED3 is illuminated
	  </para>
	</listitem>
	<listitem>
	  <para>
	    LED1 is extinguished
	  </para>
	</listitem>
	<listitem>
	  <para>
	    LED2 is extinguished
	  </para>
	</listitem>
	<listitem>
	  <para>
	    LED3 is extinguished
	  </para>
	</listitem>
      </itemizedlist>
      Each state is maintained for one second. The cycle is repeated
      five times.
      <figure float="0" id="fig-led-test">
	<title>LED test display</title>
	<mediaobject>
          <imageobject><imagedata scale="35" scalefit="1" 
          fileref="images/LEDtest.jpg" format="JPG"/>
          </imageobject>
          <textobject>
            <para>
              LED test
            </para>
          </textobject>
	</mediaobject>
      </figure>
    </para>
  </formalpara>

  <formalpara>
    <title>LCD Test</title>
    <para>
      Next, many of the features of the LCD are tested.
      <figure float="0" id="fig-lcd00">
	<title>LCD test banner</title>
	<mediaobject>
          <imageobject><imagedata scale="35" scalefit="1" 
          fileref="images/LCD00banner.jpg" format="JPG"/>
          </imageobject>
          <textobject>
            <para>
              LCD test
            </para>
          </textobject>
	</mediaobject>
      </figure>
    </para>
  </formalpara>

  <para>
    Characters are placed in all 32 positions of the display.
    <figure float="0" id="fig-lcd01">
      <title>All characters filled</title>
      <mediaobject>
        <imageobject><imagedata scale="25" scalefit="1" 
        fileref="images/LCD01fullscreen.jpg" format="JPG"/>
        </imageobject>
        <textobject>
          <para>
            LCD test
          </para>
        </textobject>
      </mediaobject>
    </figure>
  </para>


  <para>
    The display in <xref linkend="fig-lcd02" /> is displayed, but is
    built up a character at a time with a tenth second delay between
    each character.  The LCD cursor positioning capability is tested
    by drawing the top line from left to right and the lower line
    from right to left.
    <figure float="0" id="fig-lcd02">
      <title>Position cursor test</title>
      <mediaobject>
        <imageobject><imagedata scale="40" scalefit="1" 
        fileref="images/LCD02position.jpg" format="JPG"/>
        </imageobject>
        <textobject>
          <para>
            LCD test
          </para>
        </textobject>
      </mediaobject>
    </figure>
  </para>


  <para>
    The next test is for the ability to scroll the display to the
    left.  The message in <xref linkend="fig-lcd03" /> is displayed
    and then shifted to the left one position at a time with a delay
    of two-tenths of a second between each shift (<xref
    linkend="fig-lcd03" />).
    <figure float="0" id="fig-lcd03">
      <title>Shift left</title>
      <mediaobject>
        <imageobject><imagedata scale="40" scalefit="1" 
        fileref="images/LCD03shiftLeft.jpg" format="JPG"/>
        </imageobject>
        <textobject>
          <para>
            LCD test
          </para>
        </textobject>
      </mediaobject>
    </figure>
    <figure float="0" id="fig-lcd03a">
      <title>Shift Left</title>
      <mediaobject>
        <imageobject><imagedata scale="40" scalefit="1" 
        fileref="images/LCDshifLeft2.jpg" format="JPG"/>
        </imageobject>
        <textobject>
          <para>
            LCD test
          </para>
        </textobject>
      </mediaobject>
    </figure>
  </para>


  <para>
    The same thing is now done with a shift to the right.
    <figure float="0" id="fig-lcd05">
      <title>Shift Right</title>
      <mediaobject>
        <imageobject><imagedata scale="40" scalefit="1" 
        fileref="images/LCD05shiftRight.jpg" format="JPG"/>
        </imageobject>
        <textobject>
          <para>
            LCD test
          </para>
        </textobject>
      </mediaobject>
    </figure>

  </para>

  <formalpara>
    <title>Button test</title>
    <para>
      The button test requires action from the user.  A counter counts
      down from 99.  During this time, button presses are displayed on
      the LCD: <code>S1</code> - <code>U</code>, <code>S2</code> -
      <code>L</code>, <code>S3</code> - <code>R</code>,
      <code>S4</code> - <code>D</code>.  The user should also try
      pressing multiple buttons simulatneously.
      <figure float="0" id="fig-buttest1">
	<title>Button test - no buttons pressed</title>
	<mediaobject>
          <imageobject><imagedata scale="40" scalefit="1" 
          fileref="images/ButtonTest1.jpg" format="JPG"/>
          </imageobject>
          <textobject>
            <para>
              Button test
            </para>
          </textobject>
	</mediaobject>
      </figure>
      &nbsp;
      <figure float="0" id="fig-buttest2">
	<title>Button test - S1 pressed</title>
	<mediaobject>
          <imageobject><imagedata scale="45" scalefit="1" 
          fileref="images/ButtonTest2.jpg" format="JPG"/>
          </imageobject>
          <textobject>
            <para>
              Button test
            </para>
          </textobject>
	</mediaobject>
      </figure>
    </para>
  </formalpara>


  <formalpara>
    <title>Deadman test</title>
    <para>
      At this point the deadman timer is enabled and the value of the
      high word of the deadman timer is displayed.  When the deadman
      timer reaches 10000 the processor will reset.  The display is
      not synchronized with the timer so the value displayed will be
      slightly less than 10000.
      <figure float="0" id="fig-deadman">
	<title>Deadman timer test</title>
	<mediaobject>
          <imageobject><imagedata scale="35" scalefit="1" 
          fileref="images/Deadman.jpg" format="JPG"/>
          </imageobject>
          <textobject>
            <para>
              Deadman test
            </para>
          </textobject>
	</mediaobject>
      </figure>
      When the processor resets, the initial two second delay should
      cause the display to freeze for two seconds, although some white
      blocks might be displayed.  The test will restart from the
      beginning. 
    </para>
  </formalpara>


</section>
